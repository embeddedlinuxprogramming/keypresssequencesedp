//
// Created by Juan Bernardo Gómez Mendoza on 11/10/23.
//

#include <glob.h>
#include <string.h>
#include <stdio.h>
#include <linux/input.h>
#include <stdlib.h>

#include "keycode.h"

// This code is extensively based in the one found in https://stackoverflow.com/questions/58360050/detecting-keyboard-key-press-and-release-on-linux

int keycode_of_key_being_pressed() {
    FILE *kbd;
    glob_t kbddev;                                   // Glob structure for keyboard devices
    glob("/dev/input/by-path/*-kbd", 0, 0, &kbddev); // Glob select all keyboards
    int keycode = -1;                                // keycode of key being pressed
    for (int i = 0; i < kbddev.gl_pathc ; i++ ) {    // Loop through all the keyboard devices ...
        if (!(kbd = fopen(kbddev.gl_pathv[i], "r"))) { // ... and open them in turn (slow!)
            perror("Run as root to read keyboard devices");
            exit(1);
        }

        char key_map[KEY_MAX/8 + 1];          // Create a bit array the size of the number of keys
        memset(key_map, 0, sizeof(key_map));  // Fill keymap[] with zero's
        ioctl(fileno(kbd), EVIOCGKEY(sizeof(key_map)), key_map); // Read keyboard state into keymap[]
        for (int k = 0; k < KEY_MAX/8 + 1 && keycode < 0; k++) { // scan bytes in key_map[] from left to right
            for (int j = 0; j <8 ; j++) {       // scan each byte from lsb to msb
                if (key_map[k] & (1 << j)) {      // if this bit is set: key was being pressed
                    keycode = 8*k + j ;             // calculate corresponding keycode
                    break;                          // don't scan for any other keys
                }
            }
        }

        fclose(kbd);
        if (keycode)
            break;                              // don't scan for any other keyboards
    }
    return keycode;
}