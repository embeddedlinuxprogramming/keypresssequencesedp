//
// Created by Juan Bernardo Gómez Mendoza on 11/10/23.
//

#ifndef KEYPRESSSEQUENCESEDP_KEYCODE_H
#define KEYPRESSSEQUENCESEDP_KEYCODE_H

int keycode_of_key_being_pressed();

#endif //KEYPRESSSEQUENCESEDP_KEYCODE_H
