//
// Created by Juan Bernardo Gómez Mendoza on 11/9/23.
//

#include <stdio.h>
#include <unistd.h>
#include "sequences.h"

void seq1() {
    printf("\x1B[31m*     *     *     *     *     *     *     *     *     *     *     *     *   \x1B[0m\r");
    usleep(500000);
    printf("\x1B[31m   *     *     *     *     *     *     *     *     *     *     *     *      \x1B[0m\r");
    usleep(500000);
}

void seq2() {
    printf("\x1B[33m*    *    *    *    *    *    *    *    *    *    *    *    *    *    *     \x1B[0m\r");
    usleep(200000);
    printf("\x1B[33m                                                                            \x1B[0m\r");
    usleep(200000);
}

void seq3() {
    printf("\x1B[37m*    *    *    *    *    *    *    *    *    *    *    *    *    *    *    \x1B[0m\r");
    usleep(200000);
    printf("\x1B[37m *    *    *    *    *    *    *    *    *    *    *    *    *    *    *   \x1B[0m\r");
    usleep(200000);
    printf("\x1B[37m  *    *    *    *    *    *    *    *    *    *    *    *    *    *    *  \x1B[0m\r");
    usleep(200000);
    printf("\x1B[37m   *    *    *    *    *    *    *    *    *    *    *    *    *    *    * \x1B[0m\r");
    usleep(200000);
    printf("\x1B[37m    *    *    *    *    *    *    *    *    *    *    *    *    *    *    *\x1B[0m\r");
    usleep(200000);
}