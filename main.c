//
// Created by Juan Bernardo Gómez Mendoza on 11/9/23.
//

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "sequences.h"
#include "keycode.h"

void* lightTask(void* parm) {
    int* seq = parm;
    while(1) {
        if (*seq==0) seq1();
        if (*seq==1) seq2();
        if (*seq==2) seq3();
    }
}

int main(int argc, char* argv[]) {
    char c;
    setvbuf(stdout, NULL, _IONBF, 0); // Set stdout unbuffered
    int sequence = 0;
    pthread_t light;
    pthread_create(&light, NULL,
                   lightTask, &sequence);
    int key = 0;
    while (key !=45) {
        key = keycode_of_key_being_pressed();
        if (key==31) {
            sequence = (sequence+1) % 3;
            usleep(200000);
        }
    }
    printf("\n\r");
    pthread_cancel(light);
    pthread_join(light, NULL);
    return 0;
}